pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

/*
This file represents a contract that implements the following functionality:
    1. make a donation
    2. withdraw a donation to a specific address (this action can only be done by the creator of the contract)
    3. store the addresses of all users who made a donation
    4. store the amount of donations of each user

Made as part of the qualifying task for Crypton Academy

@author Gaziz Sattarov <gaziz.sattarov@gmail.com>
*/

contract owned {
    address public owner;

    constructor() public {
        owner = msg.sender;
    }

    modifier onlyOwner {
        require(owner == msg.sender);
        _;
    }
}

contract Donation is owned, ERC721 {

    uint public totalSupply;
    mapping(address => uint) public donationInfo;

    constructor() ERC721("MyNFT", "MNFT") {}

    function donate() payable public {
        totalSupply += msg.value;
        donationInfo[msg.sender] += msg.value;
    }

    function transferDonations(uint amount, address payable to) payable onlyOwner public {
        require(amount <= totalSupply, "Requested amount exceeds total amount");

        totalSupply -= amount;
        to.transfer(amount);
    }
}
