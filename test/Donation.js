const {expect} = require("chai");
var assert = require('assert')
var web3 = require('web3')

describe("Donation contract", function () {

    let hardhatDonation;
    let owner;
    let addr1;
    let addr2;
    let addrs;

    beforeEach(async function () {
        Donation = await ethers.getContractFactory("Donation");
        [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

        hardhatDonation = await Donation.deploy();
    });

    describe("Deployment", function () {
        it("Should set the right owner", async function () {
            expect(await hardhatDonation.owner()).to.equal(owner.address);
        });

        it("Should assign total supply with 0 at the start", async function () {
            const totalSupplyOnInit = 0;
            expect(await hardhatDonation.totalSupply()).to.equal(totalSupplyOnInit);
        });
    });


    describe("Transactions", function () {
        it("Should donate 50 tokens and check balance of sender", async function () {

            await hardhatDonation.connect(addr1).donate({
                value: 50
            });

            const addr1Balance = await hardhatDonation.donationInfo(addr1.address);
            expect(addr1Balance).to.equal(50);
        });

        it("Should transfer all tokens to address1", async function () {

            await hardhatDonation.donate({
                value: 1
            });
            await hardhatDonation.connect(owner).transferDonations(1, addr1.address)

            var addr1Balance = await hardhatDonation.provider.getBalance(addr1.address)
            web3.utils.toWei(
                String(addr1Balance),
                'ether'
            )
            expect(await hardhatDonation.totalSupply()).to.equal(0);
        })

        it("Should throw error because requested amount exceeds total amount", async function () {
            await hardhatDonation.donate({
                value: 40
            });

            try {
                const transferResult = await hardhatDonation.connect(owner).transferDonations(50, addr1.address)
            } catch (error) {
                err = error
            }
            assert.equal(
                err.toString(),
                'Error: VM Exception while processing transaction: reverted with reason string \'Requested amount exceeds total amount\''
            );
        })

        it("Should throw error because not owner tries to use transfer method", async function () {
            await hardhatDonation.donate({
                value: 50
            });

            try {
                const transferResult = await hardhatDonation.connect(addr1).transferDonations(50, addr2.address)
            } catch (error) {
                err = error
            }
            assert.equal(
                err.toString(),
                'Error: Transaction reverted without a reason string'
            );
        })
    });
});
